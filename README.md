#### kakoune-plantuml


[PlantUML](https://plantuml.com/) support for the [Kakoune](https://kakoune.org/) editor.  
  
In order to run PlantUML you need to have installed:  

* [Java](https://sdkman.io/) via sdkman package manager  
* [Graphviz](https://plantuml.com/graphviz-dot) (optional if you only need sequence diagrams and activity (beta) diagrams)


PlantUML installation notes [faq](https://plantuml.com/faq-install)  
The Hitchhiker’s Guide to [PlantUML](https://crashedmind.github.io/PlantUMLHitchhikersGuide/index.html)  


----


#### Install


```
mv plantuml.kak ~/.config/kak/autoload/plantuml.kak
```

[andreyorst-plug.kak](https://github.com/andreyorst/plug.kak)  
```
plug "KJ_Duncan/kakoune-plantuml.kak" domain "bitbucket.org"
```


----


Supported file extensions: `(puml|plantuml)`  
  
Use PlantUML's command line syntax to include it in your own scripts or documentation tools.  
  
First, create a text file with PlantUML commands, like this example called sequenceDiagram.puml:  
  
```
@startuml
Alice -> Bob: test
@enduml
```
  
Then run PlantUML using sequenceDiagram.puml as input. The output is an image, which either appears in the other software, or is written to an image file on disk.  
  
```
java -jar plantuml.jar sequenceDiagram.puml
```
  
This outputs your sequence diagram to a file called sequenceDiagram.png.  

![](/resources/sequenceDiagram.png)  
  
The PlantUML usage instructions have been adapted from their [Getting Started](https://plantuml.com/starting) page.  


----


#### jlink


```
$ jlink --module-path $JAVA_HOME/jmods \
        --add-modules $(jdeps --print-module-deps --ignore-missing-deps plantuml.<VERSION>.jar) \
        --output jrt-plantuml

# assumption: jrt size dependent on platform OS
$ du -sh jrt-plantuml
> 89M    jrt-plantuml

$ echo '$HOME/<PATH-TO>/jrt-plantuml/bin/java -jar $HOME/<PATH-TO>/plantuml.<VERSION>.jar "$@"' > plantuml

$ chmod -R go-rwx jrt-plantuml && \
  chmod u+x,u-w,go-rwx plantuml

$ mv plantuml $HOME/local/bin

# try this cli flag, very cool.
$ plantuml -gui &

# Platform: ubuntu wsl.exe <https://ubuntu.com/wsl>
# X11 DISPLAY: ubuntu wsl.exe <https://wiki.ubuntu.com/WSL#Running_Graphical_Applications>
# Still not working?
# Scroll to bottom, work your way up: <https://github.com/microsoft/WSL/issues/4106>
```


----


![](/resources/walker_sequence_diagram.png)


```
@startuml

participant Walker
participant Rename
participant Security
participant Numerical
participant ExceptionalMatch
participant Log
database JUnstructured
database JAnalytical

Walker -> Rename : preVisitDirectory(dir, basicFileAttributes)
  activate Rename
Rename -> Security : checkPermissions(dir)

alt  permission read/write successful
  Security --> Rename : permission granted
else permission read/write denied
  Security -> ExceptionalMatch : logger.warn(dir)
  ExceptionalMatch -> Log : exceptional()
  Log -> JUnstructured : insert(text)
  JUnstructured --> Rename : denial recorded
end

Walker -> Walker : continue
  deactivate Rename

Walker -> Rename : visitFile(path, basicFileAttributes)
  activate Rename
Rename -> Security : checkPermissions(path)

alt  permission read/write successful
  Security --> Rename : permission granted
else permission read/write denied
  Security -> ExceptionalMatch : logger.warn(path)
  ExceptionalMatch -> Log : exceptional()
  Log -> JUnstructured : insert(text)
  JUnstructured --> Rename : denial recorded
end

rnote over Walker : lookup is undetermined 
Rename --> Walker : lookup pathmatcher 
  loop N times
    Walker -> Rename : pathmatcher matches file
  end
Walker -> Walker : continue
  deactivate Rename

Walker -> Rename : visitFileFailed(Path, IOException)
  activate Rename
Rename -> ExceptionalMatch : logger.warn(path, ioException)
ExceptionalMatch -> Log : exceptional()
Log -> JUnstructured : insert(text)
JUnstructured --> Rename : exception recorded
  deactivate Rename

Walker -> Rename : postVisitDirectory(Path, IOException)
  activate Rename
Rename -> ExceptionalMatch : logger.warn(ExceptionalMatch(path, throwable))
ExceptionalMatch -> Log : exceptional() 
Log -> JUnstructured : insert(text)
JUnstructured --> Rename : exception parsed
Rename -> Numerical : statistics(time, size, total, errors)
Numerical -> JAnalytical : insert(stats)
JAnalytical --> Rename : statistics stored 
Walker -> Walker : continue
  deactivate Rename

@enduml
```


----


Kakoune default colour [codes](https://github.com/mawww/kakoune/blob/master/colors/default.kak):  

* value: red
* type,operator: yellow
* variable,module,attribute: green
* function,string,comment: cyan
* keyword: blue
* meta: magenta
* builtin: default

 
##### Kakoune General Information


Work done? Have some fun. Share any improvements, ideas or thoughts with the community [discuss.kakoune](https://discuss.kakoune.com/).  
  
Kakoune is an open source modal editor. The source code lives on github [mawww/kakoune](https://github.com/mawww/kakoune#-kakoune--).  
A discussion on the repository structure of _’community plugins’_ for Kakoune can be reviewed here: [Standardi\(s|z\)ation of plugin file-structure layout #2402](https://github.com/mawww/kakoune/issues/2402).  
Read the Kakoune wiki for information on install options via a [Plugin Manager](https://github.com/mawww/kakoune/wiki/Plugin-Managers).  
  
Thank you to the Kakoune community 230+ contributors for a great modal editor in the terminal. I use it daily for the keyboard shortcuts.  


----


That's it for the readme, anything else you may need to know just pick up a book and read it [_Polar Bookshelf_](https://getpolarized.io/). Thanks all. Bye.  
