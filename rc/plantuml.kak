# https://plantuml.com/
# ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾
# plantuml alternates:
# https://github.com/aklt/plantuml-syntax
# https://github.com/esteinberg/plantuml4idea
# -------------------------------------------------------------------------------------------------- #
# modified template from; scheme.kak, java.kak
# https://github.com/mawww/kakoune/blob/master/rc/filetype/scheme.kak
# https://github.com/mawww/kakoune/blob/master/rc/filetype/java.kak
# -------------------------------------------------------------------------------------------------- #
# kak colour codes; value:red, type,operator:yellow, variable,module,attribute:green,
#                   function,string,comment:cyan, keyword:blue, meta:magenta, builtin:default
# -------------------------------------------------------------------------------------------------- #
# Detection
# -------------------------------------------------------------------------------------------------- #
hook global BufCreate .*[.](puml|plantuml) %{
  set-option buffer filetype plantuml
}
# -------------------------------------------------------------------------------------------------- #
# Initialization
# -------------------------------------------------------------------------------------------------- #
hook global WinSetOption filetype=plantuml %{
  require-module plantuml

  set-option window static_words %opt{plantuml_static_words}

  set-option buffer comment_line "'"
  set-option buffer comment_block_begin "/'"
  set-option buffer comment_block_end "'/"

  hook window ModeChange pop:insert:.* -group plantuml-trim-indent java-trim-indent
  hook window InsertChar \n -group plantuml-indent java-indent-on-new-line
  hook window InsertChar \{ -group plantuml-indent java-indent-on-opening-curly-brace
  hook window InsertChar \} -group plantuml-indent java-indent-on-closing-curly-brace

  hook -once -always window WinSetOption filetype=.* %{ remove-hooks window plantuml-.+ }
}
# -------------------------------------------------------------------------------------------------- #
hook -group plantuml-highlight global WinSetOption filetype=plantuml %{
  add-highlighter window/plantuml ref plantuml
  hook -once -always window WinSetOption filetype=.* %{ remove-highlighter window/plantuml }
}
# -------------------------------------------------------------------------------------------------- #
provide-module plantuml %{
  require-module java
  # ------------------------------------------------------------------------------------------------ #
  # Highlighters
  # ------------------------------------------------------------------------------------------------ #
  add-highlighter shared/plantuml regions
  add-highlighter shared/plantuml/code default-region group
  add-highlighter shared/plantuml/comment region "'" "$" fill comment
  add-highlighter shared/plantuml/block region "/'" "'/" fill comment
  # ------------------------------------------------------------------------------------------------ #
  # macro: 93l<a-t><space>i<ret><esc><esc>
  evaluate-commands %sh{
    types='abstract actor agent annotation archimate artifact boundary card class cloud
      collections component control database diamond entity enum file folder frame
      hexagon interface label node object package participant person queue rectangle stack
      state storage usecase'

    keywords='across activate again allow_mixing allowmixing also alt as autonumber bold bottom
      box break caption center circle color create critical dashed deactivate description destroy
      detach dotted down else elseif empty end endif endwhile false footbox footer fork group header
      hide hnote if is italic kill left legend link loop mainframe map members namespace newpage
      normal note of on opt order over package page par partition plain ref repeat return right
      rnote rotate show skin skinparam split sprite start stereotype stop style then title top top
      to bottom direction true up while'

    declaration='@endboard @endbpm @endcreole @endcute @enddef @endditaa @enddot @endflow @endgantt
      @endgit @endjcckit @endjson @endlatex @endmath @endmindmap @endnwdiag @endproject @endsalt
      @endtree @enduml @endwbs @endwire @endyaml @startboard @startbpm @startcreole @startcute
      @startdef @startditaa @startdot @startflow @startgantt @startgit @startjcckit @startjson
      @startlatex @startmath @startmindmap @startnwdiag @startproject @startsalt @starttree
      @startuml @startwbs @startwire @startyaml'

    preprocessor='!assert !define !definelong !dump_memory !else !enddefinelong !endfunction
      !endif !endprocedure !endsub !exit !function !if !ifdef !ifndef !import !include !local !log
      !pragma !procedure !return !startsub !theme !undef !unquoted'

    skinparameter='ActivityBackgroundColor ActivityBorderColor ActivityBorderThickness ActivityDiamondFontColor
      ActivityDiamondFontName ActivityDiamondFontSize ActivityDiamondFontStyle ActivityFontColor
      ActivityFontName ActivityFontSize ActivityFontStyle ActorBackgroundColor ActorBorderColor
      ActorFontColor ActorFontName ActorFontSize ActorFontStyle ActorStereotypeFontColor
      ActorStereotypeFontName ActorStereotypeFontSize ActorStereotypeFontStyle
      AgentBorderThickness AgentFontColor AgentFontName AgentFontSize AgentFontStyle
      AgentStereotypeFontColor AgentStereotypeFontName AgentStereotypeFontSize
      AgentStereotypeFontStyle ArchimateBorderThickness ArchimateFontColor ArchimateFontName
      ArchimateFontSize ArchimateFontStyle ArchimateStereotypeFontColor
      ArchimateStereotypeFontName ArchimateStereotypeFontSize ArchimateStereotypeFontStyle
      ArrowFontColor ArrowFontName ArrowFontSize ArrowFontStyle ArrowHeadColor ArrowLollipopColor
      ArrowMessageAlignment ArrowThickness ArtifactFontColor ArtifactFontName ArtifactFontSize
      ArtifactFontStyle ArtifactStereotypeFontColor ArtifactStereotypeFontName
      ArtifactStereotypeFontSize ArtifactStereotypeFontStyle BackgroundColor
      BiddableBackgroundColor BiddableBorderColor BoundaryFontColor BoundaryFontName
      BoundaryFontSize BoundaryFontStyle BoundaryStereotypeFontColor BoundaryStereotypeFontName
      BoundaryStereotypeFontSize BoundaryStereotypeFontStyle BoxPadding CaptionFontColor
      CaptionFontName CaptionFontSize CaptionFontStyle CardBorderThickness CardFontColor
      CardFontName CardFontSize CardFontStyle CardStereotypeFontColor CardStereotypeFontName
      CardStereotypeFontSize CardStereotypeFontStyle CircledCharacterFontColor
      CircledCharacterFontName CircledCharacterFontSize CircledCharacterFontStyle
      CircledCharacterRadius ClassAttributeFontColor ClassAttributeFontName ClassAttributeFontSize
      ClassAttributeFontStyle ClassAttributeIconSize ClassBackgroundColor ClassBorderColor
      ClassBorderThickness ClassFontColor ClassFontName ClassFontSize ClassFontStyle
      ClassStereotypeFontColor ClassStereotypeFontName ClassStereotypeFontSize
      ClassStereotypeFontStyle CloudFontColor CloudFontName CloudFontSize CloudFontStyle
      CloudStereotypeFontColor CloudStereotypeFontName CloudStereotypeFontSize
      CloudStereotypeFontStyle ColorArrowSeparationSpace ComponentBorderThickness
      ComponentFontColor ComponentFontName ComponentFontSize ComponentFontStyle
      ComponentStereotypeFontColor ComponentStereotypeFontName ComponentStereotypeFontSize
      ComponentStereotypeFontStyle ComponentStyle ConditionEndStyle ConditionStyle
      ControlFontColor ControlFontName ControlFontSize ControlFontStyle ControlStereotypeFontColor
      ControlStereotypeFontName ControlStereotypeFontSize ControlStereotypeFontStyle
      DatabaseFontColor DatabaseFontName DatabaseFontSize DatabaseFontStyle
      DatabaseStereotypeFontColor DatabaseStereotypeFontName DatabaseStereotypeFontSize
      DatabaseStereotypeFontStyle DefaultFontColor DefaultFontName DefaultFontSize
      DefaultFontStyle DefaultMonospacedFontName DefaultTextAlignment DesignedBackgroundColor
      DesignedBorderColor DesignedDomainBorderThickness DesignedDomainFontColor
      DesignedDomainFontName DesignedDomainFontSize DesignedDomainFontStyle
      DesignedDomainStereotypeFontColor DesignedDomainStereotypeFontName
      DesignedDomainStereotypeFontSize DesignedDomainStereotypeFontStyle DiagramBorderColor
      DiagramBorderThickness DomainBackgroundColor DomainBorderColor DomainBorderThickness
      DomainFontColor DomainFontName DomainFontSize DomainFontStyle DomainStereotypeFontColor
      DomainStereotypeFontName DomainStereotypeFontSize DomainStereotypeFontStyle Dpi
      EntityFontColor EntityFontName EntityFontSize EntityFontStyle EntityStereotypeFontColor
      EntityStereotypeFontName EntityStereotypeFontSize EntityStereotypeFontStyle FileFontColor
      FileFontName FileFontSize FileFontStyle FileStereotypeFontColor FileStereotypeFontName
      FileStereotypeFontSize FileStereotypeFontStyle FixCircleLabelOverlapping FolderFontColor
      FolderFontName FolderFontSize FolderFontStyle FolderStereotypeFontColor
      FolderStereotypeFontName FolderStereotypeFontSize FolderStereotypeFontStyle FooterFontColor
      FooterFontName FooterFontSize FooterFontStyle FrameFontColor FrameFontName FrameFontSize
      FrameFontStyle FrameStereotypeFontColor FrameStereotypeFontName FrameStereotypeFontSize
      FrameStereotypeFontStyle GenericDisplay Guillemet Handwritten HeaderFontColor HeaderFontName
      HeaderFontSize HeaderFontStyle HexagonBorderThickness HexagonFontColor HexagonFontName
      HexagonFontSize HexagonFontStyle HexagonStereotypeFontColor HexagonStereotypeFontName
      HexagonStereotypeFontSize HexagonStereotypeFontStyle HyperlinkColor HyperlinkUnderline
      IconIEMandatoryColor IconPackageBackgroundColor IconPackageColor IconPrivateBackgroundColor
      IconPrivateColor IconProtectedBackgroundColor IconProtectedColor IconPublicBackgroundColor
      IconPublicColor InterfaceFontColor InterfaceFontName InterfaceFontSize InterfaceFontStyle
      InterfaceStereotypeFontColor InterfaceStereotypeFontName InterfaceStereotypeFontSize
      InterfaceStereotypeFontStyle LabelFontColor LabelFontName LabelFontSize LabelFontStyle
      LabelStereotypeFontColor LabelStereotypeFontName LabelStereotypeFontSize
      LabelStereotypeFontStyle LegendBorderThickness LegendFontColor LegendFontName LegendFontSize
      LegendFontStyle LexicalBackgroundColor LexicalBorderColor LifelineStrategy Linetype
      MachineBackgroundColor MachineBorderColor MachineBorderThickness MachineFontColor
      MachineFontName MachineFontSize MachineFontStyle MachineStereotypeFontColor
      MachineStereotypeFontName MachineStereotypeFontSize MachineStereotypeFontStyle
      MaxAsciiMessageLength MaxMessageSize MinClassWidth Monochrome NodeFontColor NodeFontName
      NodeFontSize NodeFontStyle NodeStereotypeFontColor NodeStereotypeFontName
      NodeStereotypeFontSize NodeStereotypeFontStyle Nodesep NoteBackgroundColor NoteBorderColor
      NoteBorderThickness NoteFontColor NoteFontName NoteFontSize NoteFontStyle NoteShadowing
      NoteTextAlignment ObjectAttributeFontColor ObjectAttributeFontName ObjectAttributeFontSize
      ObjectAttributeFontStyle ObjectBorderThickness ObjectFontColor ObjectFontName ObjectFontSize
      ObjectFontStyle ObjectStereotypeFontColor ObjectStereotypeFontName ObjectStereotypeFontSize
      ObjectStereotypeFontStyle PackageBorderThickness PackageFontColor PackageFontName
      PackageFontSize PackageFontStyle PackageStereotypeFontColor PackageStereotypeFontName
      PackageStereotypeFontSize PackageStereotypeFontStyle PackageStyle PackageTitleAlignment
      Padding PageBorderColor PageExternalColor PageMargin ParticipantFontColor
      ParticipantFontName ParticipantFontSize ParticipantFontStyle ParticipantPadding
      ParticipantStereotypeFontColor ParticipantStereotypeFontName ParticipantStereotypeFontSize
      ParticipantStereotypeFontStyle PartitionBorderThickness PartitionFontColor PartitionFontName
      PartitionFontSize PartitionFontStyle PathHoverColor PersonBorderThickness PersonFontColor
      PersonFontName PersonFontSize PersonFontStyle PersonStereotypeFontColor
      PersonStereotypeFontName PersonStereotypeFontSize PersonStereotypeFontStyle
      QueueBorderThickness QueueFontColor QueueFontName QueueFontSize QueueFontStyle
      QueueStereotypeFontColor QueueStereotypeFontName QueueStereotypeFontSize
      QueueStereotypeFontStyle Ranksep RectangleBorderThickness RectangleFontColor
      RectangleFontName RectangleFontSize RectangleFontStyle RectangleStereotypeFontColor
      RectangleStereotypeFontName RectangleStereotypeFontSize RectangleStereotypeFontStyle
      RequirementBackgroundColor RequirementBorderColor RequirementBorderThickness
      RequirementFontColor RequirementFontName RequirementFontSize RequirementFontStyle
      RequirementStereotypeFontColor RequirementStereotypeFontName RequirementStereotypeFontSize
      RequirementStereotypeFontStyle ResponseMessageBelowArrow RoundCorner SameClassWidth
      SequenceActorBorderThickness SequenceArrowThickness SequenceBoxBorderColor
      SequenceBoxFontColor SequenceBoxFontName SequenceBoxFontSize SequenceBoxFontStyle
      SequenceDelayFontColor SequenceDelayFontName SequenceDelayFontSize SequenceDelayFontStyle
      SequenceDividerBorderThickness SequenceDividerFontColor SequenceDividerFontName
      SequenceDividerFontSize SequenceDividerFontStyle SequenceGroupBodyBackgroundColor
      SequenceGroupBorderThickness SequenceGroupFontColor SequenceGroupFontName
      SequenceGroupFontSize SequenceGroupFontStyle SequenceGroupHeaderFontColor
      SequenceGroupHeaderFontName SequenceGroupHeaderFontSize SequenceGroupHeaderFontStyle
      SequenceLifeLineBorderColor SequenceLifeLineBorderThickness SequenceMessageAlignment
      SequenceMessageTextAlignment SequenceNewpageSeparatorColor SequenceParticipant
      SequenceParticipantBorderThickness SequenceReferenceAlignment
      SequenceReferenceBackgroundColor SequenceReferenceBorderThickness SequenceReferenceFontColor
      SequenceReferenceFontName SequenceReferenceFontSize SequenceReferenceFontStyle
      SequenceReferenceHeaderBackgroundColor SequenceStereotypeFontColor
      SequenceStereotypeFontName SequenceStereotypeFontSize SequenceStereotypeFontStyle Shadowing
      StackFontColor StackFontName StackFontSize StackFontStyle StackStereotypeFontColor
      StackStereotypeFontName StackStereotypeFontSize StackStereotypeFontStyle
      StateAttributeFontColor StateAttributeFontName StateAttributeFontSize
      StateAttributeFontStyle StateBorderColor StateFontColor StateFontName StateFontSize
      StateFontStyle StateMessageAlignment StereotypePosition StorageFontColor StorageFontName
      StorageFontSize StorageFontStyle StorageStereotypeFontColor StorageStereotypeFontName
      StorageStereotypeFontSize StorageStereotypeFontStyle Style SvglinkTarget
      SwimlaneBorderThickness SwimlaneTitleFontColor SwimlaneTitleFontName SwimlaneTitleFontSize
      SwimlaneTitleFontStyle SwimlaneWidth SwimlaneWrapTitleWidth TabSize TimingFontColor
      TimingFontName TimingFontSize TimingFontStyle TitleBorderRoundCorner TitleBorderThickness
      TitleFontColor TitleFontName TitleFontSize TitleFontStyle UsecaseBorderThickness
      UsecaseFontColor UsecaseFontName UsecaseFontSize UsecaseFontStyle UsecaseStereotypeFontColor
      UsecaseStereotypeFontName UsecaseStereotypeFontSize UsecaseStereotypeFontStyle WrapWidth'

    plantumlcolour='APPLICATION AliceBlue AntiqueWhite Aqua Aquamarine Azure BUSINESS Beige
      Bisque Black BlanchedAlmond Blue BlueViolet Brown BurlyWood CadetBlue Chartreuse Chocolate
      Coral CornflowerBlue Cornsilk Crimson Cyan DarkBlue DarkCyan DarkGoldenRod DarkGray
      DarkGreen DarkGrey DarkKhaki DarkMagenta DarkOliveGreen DarkOrchid DarkRed DarkSalmon
      DarkSeaGreen DarkSlateBlue DarkSlateGray DarkSlateGrey DarkTurquoise DarkViolet Darkorange
      DeepPink DeepSkyBlue DimGray DimGrey DodgerBlue FireBrick FloralWhite ForestGreen Fuchsia
      Gainsboro GhostWhite Gold GoldenRod Gray Green GreenYellow Grey HoneyDew HotPink
      IMPLEMENTATION IndianRed Indigo Ivory Khaki Lavender LavenderBlush LawnGreen LemonChiffon
      LightBlue LightCoral LightCyan LightGoldenRodYellow LightGray LightGreen LightGrey LightPink
      LightSalmon LightSeaGreen LightSkyBlue LightSlateGray LightSlateGrey LightSteelBlue
      LightYellow Lime LimeGreen Linen MOTIVATION Magenta Maroon MediumAquaMarine MediumBlue
      MediumOrchid MediumPurple MediumSeaGreen MediumSlateBlue MediumSpringGreen MediumTurquoise
      MediumVioletRed MidnightBlue MintCream MistyRose Moccasin NavajoWhite Navy OldLace Olive
      OliveDrab Orange OrangeRed Orchid PHYSICAL PaleGoldenRod PaleGreen PaleTurquoise
      PaleVioletRed PapayaWhip PeachPuff Peru Pink Plum PowderBlue Purple Red RosyBrown RoyalBlue
      STRATEGY SaddleBrown Salmon SandyBrown SeaGreen SeaShell Sienna Silver SkyBlue SlateBlue
      SlateGray SlateGrey Snow SpringGreen SteelBlue TECHNOLOGY Tan Teal Thistle Tomato Turquoise
      Violet Wheat White WhiteSmoke Yellow YellowGreen'

    # -------------------------------------------------------------------------------------------- #
    # c-family.kak: <https://github.com/mawww/kakoune/blob/master/rc/filetype/c-family.kak#L271>
    join() { sep=$2; eval set -- $1; IFS="$sep"; echo "$*"; }
    # -------------------------------------------------------------------------------------------- #
    add_highlighter() { printf "add-highlighter shared/plantuml/code/ regex %s %s\n" "$1" "$2"; }
    # -------------------------------------------------------------------------------------------- #
    add_start_word_highlighter() {

      while [ $# -gt 0 ]; do
        words=$1 face=$2; shift 2
        regex="^($(join "${words}" '|'))\\b"
        add_highlighter "$regex" "1:$face"
      done

    }
    # -------------------------------------------------------------------------------------------- #
    # alexherbo2: <https://github.com/alexherbo2/plug.kak/blob/master/rc/plug.kak#L108>
    add_word_highlighter() {

      while [ $# -gt 0 ]; do
        words=$1 face=$2; shift 2
        regex="\\b($(join "${words}" '|'))\\b(?=[\\s\\n])"
        add_highlighter "$regex" "1:$face"
      done

    }
    # -------------------------------------------------------------------------------------------- #
    # lesson learnt: style macro in for loop use: $@ not "$@"
    print_static_words() {

      for name in $@
      do
        printf " %s" "$name"
      done

    }
    # -------------------------------------------------------------------------------------------- #
    printf "declare-option str-list plantuml_static_words "

    print_static_words "$types" "$keywords" "$declaration" "$preprocessor" "$skinparameter" "$plantumlcolour"

    printf "\n"
    # -------------------------------------------------------------------------------------------- #
    add_word_highlighter "$types" "type" "$keywords" "keyword" "$skinparameter" "variable" "$plantumlcolour" "meta"
    add_start_word_highlighter "$declaration" "keyword" "$preprocessor" "value"

    printf "\n"
    # -------------------------------------------------------------------------------------------- #
  }
}
